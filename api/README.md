## Introduction to Task API RESTFULL
1. When the customer access to the Task API, this has `customer` role for default.
2. If the customer wants to register in the app, This has two ways: 
    1. Following the OAuth2 authorization with google or github.
    2. Following the form registration. 

3. When the client access to the Task API first time, this blank properties in the `model` for default, or we can save `cookies` information in the session storage web while the customer register his data folliwing the steep 2.

📝✅🔐

This customer has only read-only access to the app. Wheather this wants to to any changes in the app, this need to register as `admin` user role first.

