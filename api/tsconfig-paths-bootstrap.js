// tsconfig-paths-bootstrap.js
const tsConfigPaths = require('tsconfig-paths');
const tsConfig = require('./tsconfig.json');

tsConfigPaths.register({
    baseUrl: './dist', // Igual que en tsconfig.json
    paths: tsConfig.compilerOptions.paths || {},
});

require('ts-node').register({
    project: 'tsconfig.json',
});