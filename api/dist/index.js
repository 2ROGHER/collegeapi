/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/controllers/HelloController.ts":
/*!********************************************!*\
  !*** ./src/controllers/HelloController.ts ***!
  \********************************************/
/***/ (function(__unused_webpack_module, exports) {

eval("\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nexports.HelloController = void 0;\n// Implements the IHelloController\nclass HelloController {\n    // NO atributes\n    getMessage(name) {\n        return __awaiter(this, void 0, void 0, function* () {\n            // A simple JSON response\n            return {\n                message: `Hello ${name}, Wellcome my API. Thanks for visit`,\n            };\n        });\n    }\n    ;\n}\nexports.HelloController = HelloController;\n;\n\n\n//# sourceURL=webpack://collegeapi/./src/controllers/HelloController.ts?");

/***/ }),

/***/ "./src/controllers/TestController.ts":
/*!*******************************************!*\
  !*** ./src/controllers/TestController.ts ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, exports) {

eval("\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nexports.TestController = void 0;\nclass TestController {\n    response(message) {\n        return __awaiter(this, void 0, void 0, function* () {\n            return {\n                message: \"Esto no es un error\" + message,\n            };\n        });\n    }\n}\nexports.TestController = TestController;\n\n\n//# sourceURL=webpack://collegeapi/./src/controllers/TestController.ts?");

/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst dotenv_1 = __importDefault(__webpack_require__(/*! dotenv */ \"dotenv\"));\nconst index_1 = __importDefault(__webpack_require__(/*! ./server/index */ \"./src/server/index.ts\"));\n// Config environment variables\ndotenv_1.default.config();\n// Get the port from the environment\nconst PORT = process.env.DB_PORT || 8000;\n// Execute the express app\nindex_1.default.listen(PORT, () => {\n    console.log(\"Server on in: \", PORT);\n});\n// Control server error here.\nindex_1.default.on(\"error\", (error) => {\n    console.log(\"The error is: \", error.all);\n});\n\n\n//# sourceURL=webpack://collegeapi/./src/index.ts?");

/***/ }),

/***/ "./src/routes/HelloRoute.ts":
/*!**********************************!*\
  !*** ./src/routes/HelloRoute.ts ***!
  \**********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\n// We need to import the Controller \nconst HelloController_1 = __webpack_require__(/*! ../controllers/HelloController */ \"./src/controllers/HelloController.ts\");\n// Let's make the route to manage \nlet helloRoute = express_1.default.Router();\nhelloRoute.route('/')\n    .get((req, res) => __awaiter(void 0, void 0, void 0, function* () {\n    var _a;\n    console.log('hello this code is running');\n    // 1. Get the query param that is passed throught URL\n    let name = (_a = req === null || req === void 0 ? void 0 : req.query) === null || _a === void 0 ? void 0 : _a.name;\n    // 2. Lets create a Controller Instance\n    let controller = new HelloController_1.HelloController();\n    // 3. Obtain the response\n    const response = yield controller.getMessage(name);\n    // 4. Send the response to client\n    return res.send(response);\n}));\n//export the route\nexports[\"default\"] = helloRoute;\n\n\n//# sourceURL=webpack://collegeapi/./src/routes/HelloRoute.ts?");

/***/ }),

/***/ "./src/routes/TestRoute.ts":
/*!*********************************!*\
  !*** ./src/routes/TestRoute.ts ***!
  \*********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\nconst TestController_1 = __webpack_require__(/*! ../controllers/TestController */ \"./src/controllers/TestController.ts\");\nlet testRoute = express_1.default.Router();\ntestRoute.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {\n    let message = 'este es un mesanje';\n    let controller = new TestController_1.TestController();\n    let response = yield controller.response(message);\n    return res.json(response);\n}));\nexports[\"default\"] = testRoute;\n\n\n//# sourceURL=webpack://collegeapi/./src/routes/TestRoute.ts?");

/***/ }),

/***/ "./src/routes/index.ts":
/*!*****************************!*\
  !*** ./src/routes/index.ts ***!
  \*****************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\n// Here goes all routes\nconst express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\n// Import all routes here\nconst HelloRoute_1 = __importDefault(__webpack_require__(/*! ./HelloRoute */ \"./src/routes/HelloRoute.ts\"));\nconst TestRoute_1 = __importDefault(__webpack_require__(/*! ./TestRoute */ \"./src/routes/TestRoute.ts\"));\n// LEt's make a express instance\nlet server = (0, express_1.default)();\n// We need to craete the 'main' route to works\nlet rootRooter = express_1.default.Router();\n// Activate for all request here the route in the link: 'http://localhost:8000/api'\nrootRooter.get('/', (req, res) => {\n    res.send(\"Wellcome to College API\");\n});\n// MAIN ROUTE\nserver.use(\"/\", rootRooter);\n// ****** HERE ARE GOING ALL NO MAIN ROUTES ********\nserver.use('/hello', HelloRoute_1.default);\nserver.use('/test', TestRoute_1.default);\n// Export the main route\nexports[\"default\"] = rootRooter;\n\n\n//# sourceURL=webpack://collegeapi/./src/routes/index.ts?");

/***/ }),

/***/ "./src/server/index.ts":
/*!*****************************!*\
  !*** ./src/server/index.ts ***!
  \*****************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\nconst dotenv_1 = __importDefault(__webpack_require__(/*! dotenv */ \"dotenv\"));\nconst routes_1 = __importDefault(__webpack_require__(/*! ../routes */ \"./src/routes/index.ts\"));\n// Middlewares\nconst cors_1 = __importDefault(__webpack_require__(/*! cors */ \"cors\"));\nconst helmet_1 = __importDefault(__webpack_require__(/*! helmet */ \"helmet\"));\nconst TestRoute_1 = __importDefault(__webpack_require__(/*! ../routes/TestRoute */ \"./src/routes/TestRoute.ts\"));\n// Environments variables\ndotenv_1.default.config();\n// Create the express server aplication\nlet server = (0, express_1.default)();\n// Define the entry route or the main 'end-point' to acces the resource\nserver.use(\"/api\", routes_1.default); // http://localhost:8000/api/\nserver.use('/test', TestRoute_1.default);\n// Define the route for static files\nserver.use(express_1.default.static(\"public\"));\n// TODO: Conexion with database\n// Implements security methods to app\nserver.use((0, helmet_1.default)());\nserver.use((0, cors_1.default)());\n// We need to limit the comunication for improve performance\nserver.use(express_1.default.urlencoded({ extended: true, limit: \"50mb\" }));\n// We need to specify the response type\nserver.use(express_1.default.json({ limit: \"50mb\" }));\n// Now we need to redirect the route: 'http://localhost:8000/api' when the fail the route\nserver.get(\"/\", (req, res) => {\n    res.redirect(\"/api\");\n});\n// We need to export the server to --> main.ts\nexports[\"default\"] = server;\n\n\n//# sourceURL=webpack://collegeapi/./src/server/index.ts?");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("cors");

/***/ }),

/***/ "dotenv":
/*!*************************!*\
  !*** external "dotenv" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("dotenv");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("express");

/***/ }),

/***/ "helmet":
/*!*************************!*\
  !*** external "helmet" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("helmet");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.ts");
/******/ 	
/******/ })()
;