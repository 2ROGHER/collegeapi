## 1. Usefull commands in the shell of PGSQ
```cmd
>[cmd]: psql -U postgres 
pwd: 

> \l : #listar la base de datos
> \connect | \c [database_name] #connect to database
> \dt: muestra todas las tablas de la base de datos
> \d [table_name]: muestra la estructura de la tabla de datos de nombre que se usa

> \du : Lista todos los roles de usuarios disponibles en el servidor postgresql
> \q : Sale de la consola de postgresql
> \? : Muestra la lista de comandos utilis de postgresql
> \password: Cambia la constrsenia del usuario actual
> \timing: activa o desactiva el cronometro para medir el tiempo de ejecucion de las consultas
> \conninfo: Muestra información sobre la base de datos actual de postgresql.

 ```

## Mongo Commands
```mongosh
$ db.getCollectionNames() #Get all collections from database.
$ db.collection_name.find() #Find a collection regitrared.
```

## 2. Instalaciones para trabajar con Sequelize(ORM) & PostgreSQL(DB)
```bash  
$ npm install -S sequelize pg pg-store
```