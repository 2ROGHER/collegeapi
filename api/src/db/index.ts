// Here we type for the connection with the DB
import mongoose from "mongoose";
// import env variables from .dotenv
import dotenv from "dotenv";
dotenv.config();

/**
 * This class is used to connect to the database
 * 
 */
class Connection {
  // public method to connect to the database mongodb.
  public connect(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      try {
        resolve({ connection: mongoose.connect(`mongodb://localhost:27017/taskdb`)});
      } catch (error) {
        reject({ message: `Something went wrong in connect to database ${error}`})
      }
    });
  }
}

export default Connection;
