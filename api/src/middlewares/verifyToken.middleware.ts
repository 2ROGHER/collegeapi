import jwt from "jsonwebtoken";
import { Response, Request, NextFunction } from "express";

import dotenv from "dotenv";
import { LogError } from "../utils/Logger";

// Config environment variables
dotenv.config();

// Secret key
const secret = process.env.SECRET_KEY || "MISECRETKEY";

/**
 * Here we are creating a new middleware to protect the routes, until the 
 * Client is registered en the app.
 * @param req Request object
 * @param res Response object
 * @param next  Next object
 * @returns JSON object
 */

export const verifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Check the headers from requests for "x-access-token";
  // Need to send the jwt token through the header-request.
  let jwtToken: any = req.headers["x-access-token"];

  // Verify whether token is valid or no [null or present]
  if (!jwtToken) {
    LogError(403, "Failed to verify token to access the endpoint", req.url);
    return res.status(403).send({
      authenticationError: "Failed to verify JWT token",
      message: "Not authorized to consume this endpoint",
    });
  }

  jwt.verify(jwtToken, secret, (err: any, decoded: any) => {
    if (err) {
      LogError(
        500,
        "Authentication Error, JWT verification token failed",
        req.url
      );
      return res.status(400).send({
        authenticationError: "JWT verification token failed",
        message: "Failed to verify JWT token",
      });
    }
  });

  next();
};
