enum ROLE {
    // Advanced role HIGH level user: read, write, delete, update, managment.
    ADMIN = "admin",

    // Completely access properties: read, write, delete, update, etc.
    SUPER = "super",

    // Code insider properties:
    DEVELOPER = "developer",
    EDITOR = "editor",

    // Read and edit properties: read and write
    MODERATOR = "moderator",
    MANAGER = "manager",
    SUPPORT = "support",

    // Read-only properties: read only.
    GUEST = "guest",
    CUSTOMER = "customer", // can be USER too.
    AUDITOR = "auditor",
}

export default ROLE;
