import mongoose, { Schema } from "mongoose";
import { IUser } from "../interfaces/IUser.interface";
import ROLE from "../types/Role.enum";
import { UUID } from "sequelize";
const UserEnity = (): any => {
    // We need to create a new schema.
    let userSchema = new mongoose.Schema<IUser>({
        // id: it's done ! ✅
        // id: {
        //     type: UUID,
        // },
        username: {
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: true,
        },

        // Each user has a several Roles properties.
        role: {
            type: String,
            required: true,
        }
        

    });

    let UserModel = mongoose.models.User || mongoose.model<IUser>("User", userSchema);
    return { UserModel, userSchema };
};

export default UserEnity;