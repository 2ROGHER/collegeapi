// import the IUser.interface.ts to implement all here
import { ICustomer } from "../interfaces/ICustomer.interface";
import mongoose, { Schema } from "mongoose";
import UserEnity from "./User.entity";

// Whe need to user the userSchema as Type for "User" atribute
// to nested this two entities.
let userSchema = UserEnity().userSchema;

const CustomerEntity = (): any => {
  let customerSchema = new mongoose.Schema<ICustomer>({
    name: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    DOB: {
      type: Date,
      required: true,
    },
    age: {
      type: Number,
      required: true,
    },
    user: {
      type: userSchema,
      required: false,
    }
   
    // here we need to implement the relationships
    // any user has a lot of task to do.
    // tasks: {
    //   type: [],
    //   required: true,
    // }
  });

  // Here we need to specify mongoose if exists a collection with name "User"
  // use it and insteand create one more.
  return mongoose.models.Customer || mongoose.model<ICustomer>("Customer", customerSchema);

};

export default CustomerEntity;
