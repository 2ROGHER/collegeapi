// Here we need to implement the model for tasks
import mongoose, { Schema } from "mongoose";
import { ITask } from "../interfaces/ITask.interface";

const TaskEntity = (): any => {
  let taskSchema = new mongoose.Schema<ITask>({
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    date: {
      type: Date,
      required: false,
      default: Date.now,
    },
    tag: {
      type: String,
      required: true,
    },
    properties: {
      type: [String],
      required: true,
    },
    author: {
      type: Schema.Types.ObjectId,
      ref: "Client",
      required: true,
    }
  },
  // Each instance must have methods
  
  );

  return mongoose.models.Task || mongoose.model<ITask>("Task", taskSchema); // mongose.model<ITask>("Task", taskSechema)
};

export default TaskEntity;
