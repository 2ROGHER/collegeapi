// Here let's create a new interface for tasks
import { Document, Schema, Types } from 'mongoose';

export interface ITask extends Document {
  id?: string;
  title: string;
  description: string;
  status: string;
  date: Date;
  tag: string;
  properties: [string];
  // user ID
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Client',
    required: true,
  };
};
