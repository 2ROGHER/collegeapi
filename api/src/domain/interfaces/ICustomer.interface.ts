import { Document, Schema } from "mongoose";
import { ITask } from "./ITask.interface";
import { IUser } from "./IUser.interface";
// Let's to create a inteface for define a user
export interface ICustomer extends Document {
  id?: string;
  name: string;
  lastName: string;
  phone: string;
  email: string;
  DOB: Date;
  age: number;
  // userId: {
  //   type: Schema.Types.ObjectId,
  //   ref: "User"
  // }
  user: IUser;
  // here we need to implement the relationships
  // any user has a lot of task to do.
  // TODO: implement relationships here.
  // tasks: []
}
