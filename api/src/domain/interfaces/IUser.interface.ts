import { Document, Schema, Types } from "mongoose";
import ROLE from "../types/Role.enum";
import { UUID } from "crypto";

export interface IUser  {
    _id?: Types.ObjectId;
    username: string;
    password: string;
    role: Enumerator;
}