import { Document } from 'mongoose';

export interface IAuth {
    username: string;
    password: string;
}