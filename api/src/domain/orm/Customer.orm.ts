// This is the file that we use to send information to CLIENT
import { LogError, LogSuccess, LogWarning } from "../../utils/Logger";
import { IAuth } from "../interfaces/IAuth.interface";
import { IUser } from "../interfaces/IUser.interface";
import TaskEntity from "../models/Task.entity";
import UserEntity from "../models/Customer.entity";

import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';


import dotenv from 'dotenv';
import { ICustomer } from "../interfaces/ICustomer.interface";
import { pagination } from "../../services/pagination.service";
import CustomerEntity from "../models/Customer.entity";

dotenv.config();

let secret = process.env.SECRET_KEY || 'MYSECRETKEY';

// Method ORM to  create a new USER in DB
export const CreateCustomer = async (data: ICustomer): Promise<any> => {
  let CustomerModel: any = CustomerEntity(); // Invoqute the function thats returna  Schema
  // save data
  try {
    return await CustomerModel.create(data);
    // .then((res: any) => {
    //   LogSuccess(
    //     200,
    //     "Create new Client process successfully" + res,
    //     "/api/Client"
    //   );
    // })
    // .catch((error: any) => {
    //   LogError(500, error, "/api/Client");
    // });
  } catch (error: any) {
    LogError(
      500,
      ("Creating new client in the DB proccess failed" + error) as string,
      "/api/tasks/"
    );
    // throw new error to handle in controller
    throw new Error("Something went wrong while creating new client" + error.message);
  }
};

// Method ORM to get all Clients from DB
export const GetAllCustomers = async (
  page?: number,
  limit?: number
): Promise<any> => {
  try {
    let CustomerModel: any = CustomerEntity(); // Let's create an instance of the Client model

    let response: any = {}; // JSON type

    // Find Clients with pagination
    // const Clients = await ClientModel.find({ isDelete: false })
    // .select("name lastName email phone Clientname password")
    // .limit(limit)
    // .skip((page - 1) * limit)
    // .exec();

    let customers = await CustomerModel.find() // return a list of clients from db.
      .limit(limit)
      .exec();

    if (!customers) {
      // Handle case when no clients found
      response.clients = {
        message: "Empty customers list",
        clients: [],
      };
      LogWarning(204, "No customer found in database.", "/api/v1/clients");
    } 

    // Get paginations.
    response = await pagination(page, limit);
    response.customers = customers;
    
    // Return the response in JSON format to client
    return response;
  } catch (error: any) {
    // Handle errors properly
    LogError(500, "Request failed to get all customers from database", "/api/v1/customers");
    throw new Error("Get all customers request failed: " + error.message); // Throw new  error to be caught by the caller
  }
};

// Method ORM to get customers by ID from DB.
export const GetCustomerByID = async (id: string): Promise<any> => {
  try {
    // Now we need to create a instance of schema model
    let CustomerModel = CustomerEntity();
    return await CustomerModel.findById({ _id: id })
      .select("name lastName email phonea age") // this depends clients requirements.
      .exec();
  } catch (error: any) {
    LogError(500, "Get Client by ID request wrong: " + error, "/api/v1/clients/" + id);
    throw new Error("Get Client by ID request wrong: " + error.message)
  }
};

// Method ORM to delete client by ID from DB.
export const DeleteCustomerByID = async (id: string): Promise<any> => {
  try {
    let UserModel = UserEntity();
    // here we need to delete an user
    return await UserModel.deleteOne({ _id: id });
  } catch (error: any) {
    LogError(500, "Delete user by ID wrong" + error, "/api/v1/clients/" + id);
    throw new Error("Delete user by ID request failed" + error.message);
  }
};

// Update User
export const UpdateCustomer = async (customer: ICustomer, id: string): Promise<any> => {
  let UserModel = UserEntity();
  try {
    // here we need to update the user in db with new data passed by parameter
    // matched with 'id'
    return await UserModel.findByIdAndUpdate(id, customer, { new: true });
  } catch (error: any) {
    LogError(500, "Update customer request failed." + error, "/api/v1/customers/" + id);
    throw new Error("Update customer request failed" + error.message);
  }
};

// Get task by user id
export const GetTaskByCustomerID = async (id: string): Promise<any> => {
  let TaskModel = TaskEntity();
  try {
    return await TaskModel.find({ author: id });
  } catch (error: any) {
    LogError(500, "[Request] Get tasks by User ID failed: " + error.message, "/api/users/tasks?id=" + id);
    throw new Error("Something went wrong" + error.message);
  }
};
