import mongoose from "mongoose";
import TaskEntity from "../models/Task.entity";
import { ITask } from "../interfaces/ITask.interface";
import { LogError } from "../../utils/Logger";
// Create a new Task.
export const CreateTask = async (task: ITask): Promise<any> => {
  let TaskModel: any = TaskEntity(); // We need to create a intance from TaskEntity class.
  try {
    //create task with mongoose ORM
    return await TaskModel.create(task);
  } catch (error: any) {
    LogError(
      error.code,
      ("[Request] create task failed" + error) as string,
      "/api/tasks/"
    );
    throw new Error("Something went wrong: " + error.message);
  }
};

export const GetAllTasks = async (id?: string): Promise<any> => {
  let TaskModel: any = TaskEntity();
  try {
    //Get all task form db
    return await TaskModel.find();
  } catch (error: any) {
    LogError(
      500,
      ("[Request] Get all tasks failed: " + error) as string,
      "/api/tasks/"
    );
    throw new Error("Something went wrong: " + error.message);
  }
};

export const GetTaskByID = async (id?: string): Promise<any> => {
  let TaskModel: any = TaskEntity();
  try {
    return await TaskModel.findById(id);
  } catch (error: any) {
    LogError(
      500,
      ("[Request] Get task by ID failed: " + error) as string,
      "/api/tasks?id=" + id
    );
    throw new Error("Something went wrong: " + error.message);
  }
};
