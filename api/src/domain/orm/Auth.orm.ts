import { LogError } from "../../utils/Logger";
import { IAuth } from "../interfaces/IAuth.interface";
import { IUser } from "../interfaces/IUser.interface";
import UserEntity from "../models/User.entity";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { ICustomer } from "../interfaces/ICustomer.interface";
import CustomerEntity from "../models/Customer.entity";
import { CreateCustomer } from "./Customer.orm";
import { CreateUser } from "./User.orm";
import { JsonObject } from "swagger-ui-express";

dotenv.config();

let secret = process.env.SECRET_KEY || "MISECRETKEY";

var CustomerModel = CustomerEntity();

export const Login = async (auth: IAuth): Promise<any> => {
    // Here we neet to implemented the login process.
    try {
        let customerFound: ICustomer | undefined;
        let token: string | undefined;
        let validPassword: boolean | undefined;

        // Check whether User exists by unique username (validaton failed).
        customerFound = await CustomerModel.findOne({
            'user.username': auth.username
        })
            .exec();

        if (customerFound) {
            // Whether user exists validate user password and encrypt the credentials.
            validPassword = bcrypt.compareSync(
                auth.password,
                customerFound!.user.password
            );

            // Check whether password is valid (compare with bcrypt)
            if (!validPassword) {
                // TODO: handle error here for validatons process.
                throw new Error(`Something went wrong. Invalid password, try again`);
            }

            // Whether all is valid we need to create a jwt token and generete to send to customer
            token = jwt.sign(
                { username: customerFound!.user.username },
                secret,
                {
                    expiresIn: "1h",
                }
            );
        } else {
            // TOOD: handle error correctly
            throw new Error(`Username ` + auth.username + ` is not valid or not found`);
        }

        // Then we need to return this token to customer
        return {
            username: customerFound!.user.username || "Error",
            token: token || "Error",
        };
    } catch (error: any) {
        // TODO: Handle error here properly.
        throw new Error(
            "Error: " + error.message
        );
    }
};

export const Register = async (customer: ICustomer): Promise<any> => {
    let response: JsonObject | any = {};
    try {
        // Search if the customer exists in the database
        let customerExists = await CustomerModel.findOne({ name: customer.name});

        if (customerExists) {
            LogError(
                409,
                "Customer already exists in database",
                "/api/v1/auth/register"
            );
            throw new Error("Customer already exists in database");
        }

        

        // Create the new customer or register it.
        response =  await CreateCustomer(customer);

        let customerCreated = await CustomerModel.findOne({ name: customer.name }).exec();

        // Create the new user to linked with this customer.
        let newUser: IUser = {
           _id: customerCreated.user._id, 
           username: customerCreated.user.username,
           password: customerCreated.user.password,
           role: customerCreated.user.role,
        };

        // Create the new user
        await CreateUser(newUser);

        return response;

    } catch (error) {
        // TODO: Manage error here properly.
        throw new Error("Registration process failed: " + error);
    }
};

export const Logout = async (): Promise<any | undefined> => {};
