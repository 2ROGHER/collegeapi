import { UUID } from "crypto";
import UserEnity from "../models/User.entity";
import { IUser } from "../interfaces/IUser.interface";
import { LogError } from "../../utils/Logger";

let UserModel = UserEnity().UserModel;

export const GetAllUsers = async (): Promise<any> => {

    try {
        return await UserModel.find();
    } catch (error) { 
        throw new Error(
            "An error ocurred while trying to get all users " + error
        );
    }
};
export const CreateUser = async (user: IUser): Promise<any> => {
    try {
        return await UserModel.create(user);
    } catch (error: any) {
        LogError(
            500,
            "Create user process failed",
            "/api/v1/users"
        );
        throw new Error("An error ocurred while trying to create user: " + error.message);
    }
};

export const UpdateUserByID = async (id: UUID): Promise<any> => {
    return null;
};

export const DeleteUserByID = async (id: UUID): Promise<any> => {
    return null;
};

export const GetUserById = async (id: UUID): Promise<any> => {
    throw new Error("An error ocurred while trying to");
};
