import dotenv from "dotenv";
import server from "./server/index";
// import the connection function to connect the database
import connection from "./db/index";
import chalk from "chalk";
import Connection from "./db/index";

// Config environment variables
dotenv.config();
// Get the port from the environment
const PORT: String | number = process.env.PORT || 8000;

// Connection with the Database
new Connection()
  .connect()
  .then(async (res) => {
    console.log(chalk.rgb(255, 255, 255)(`
      
           /\\
          /  \\ 
          |==|
          |  |
          |  |
         /____\\
       /|      |\\
      | |  |   | |
      |/|  |   |\\|
        |__|___|
         [_][_]

         #/\\ /\\#
        ||||||||
        || || ||
        || || ||
     """"######""""
   """""########"""""
 """"""##########""""""
 
        RUNNING!.
    `));
  })
  .then((res) => {
    // Execute the express app
    server.listen(PORT, () => {
      console.log("+[Server is running & ready 🤖] | [port]:", PORT);
    });
  })
  .catch((err) => {
    console.log(`-[${err.message}]`);
  });

// Control server error here.
server.on("error", (error) => {
  console.log("The error is: ", error.all);
});
