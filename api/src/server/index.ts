import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

//import the main route
import rootRouter from "../routes";

// Import  Middlewares
import cors from "cors";
import helmet from "helmet";
// TODO: HTTPS methods to improve the app  security

// Import LOGS here
import { LogError } from "../utils/Logger";
import chalk from "chalk";

// Environments variables
dotenv.config();

// Create the express server aplication
let server: Express = express();

/***** Define and apply own Middlewares **** */

server.use((req: Request, res: Response, next) => {
  console.log(`${chalk.rgb(174, 226, 169)("[Middleware is running successfully]")}`);
  // TODO: Implementes own security middlewares to protect the routes.
  next();
});

// This middleware is responsible for send to cliente all request for 'static' resources
// http://localhost:8000/static/
server.use(
  "/static",
  express.static(__dirname.split("\\").splice(0, 6).join("\\") + "\\public\\")
);
// server.use(express.static("../../public/"));


// Implements security middlewares features to protect application.
server.use(helmet());
// we need to implement the cors() middleware fot all request to allow it to makes requests any other endpoints.
server.use(cors());
//Habilito CORS para una ruta en particular
// app.get('/example-route', cors(),  (req, res, next) => {
//     res.sendStatus(200)
// })

// We need to limit the comunication for improve performance
server.use(express.urlencoded({ extended: true, limit: "50mb" }));

// Specity response type comunication between client and server.
server.use(express.json({ limit: "50mb" }));

// Define the entry route or the main 'end-point' to acces the resource
server.use("/api/v1/", rootRouter); // http://localhost:8000/api/

// Response when the route not match with any ruoute client!
server.get("*", (req: Request, res: Response) => {
  res.status(400).send("Request failed: " + res.statusCode);
  LogError(
    res.statusCode,
    ("Request failed in entry root") as string,
     req.url
  );

  // Redirect route other handlers
  // res.redirect("/api/v1/bad-request");
});

// Now we need to redirect the route: 'http://localhost:8000/api/v1/default' when the fail the route request client.
server.get("/", (req: Request, res: Response) => {
  res.redirect("/api/v1/index"); // esta ruta si va
});



// We need to export the server to --> main.ts
export default server;
