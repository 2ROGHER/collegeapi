import { ITask } from "../domain/interfaces/ITask.interface";
import { ITaskController } from "./interfaces";
import { GetAllTasks, CreateTask, GetTaskByID } from "../domain/orm/Task.orm";
import { LogError } from "../utils/Logger";
// Este controller se encarga de implementar los metodos del ORM para
// realizar la operaciones correspondientes con los modelos de datos en la DB.
// returna una "response" para que en las rutas se controlen y se envien al cliente.
export class TaskController implements ITaskController {
  public async deleteTaskByID(id: string): Promise<ITask> {
    throw new Error("Method not implemented.");
  }

  public async getTaskByID(id?: string): Promise<any> {
    try {
      return await GetTaskByID(id);
    } catch (error: any) {
      LogError(
        500,
        ("[Request] Get task by ID failed: " + error) as string,
        "/api/tasks/"
      );
       throw new Error(error.message);
    }
  }

  public async createTask(task: ITask): Promise<any> {
    try {
      return await CreateTask(task); // aqui veo si fun ciona o no
    } catch (error: any) {
      // If doesn't exist any field in the task schema, the Model throws a error.
      LogError(400, "[Request] Create  task failed: " + error, "/api/tasks/");
      throw new Error(error.message); // we need to throw the error.
    }
  }

  public updateTask(task: ITask, id: string): Promise<any> {
    throw new Error("Method not implemented.");
  }

  public async getAllTasks(): Promise<any> {
    try {
      return await GetAllTasks();
    } catch (error: any) {
      LogError(500, "[Request] Get all tasks failed: " + error, "/api/tasks/");
      throw new Error(error.message);
    }
  }
}
