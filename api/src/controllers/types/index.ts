// This file allow us to define basic to response to replace that 
// 'any' statements in controllers

/**
  * Basic JSON response for controllers that returns a  JSON object
  * @returns { JSON } message
 */

export type BasicResponse = {
    message: string;
};

/**
  * Basic JSON response for controllers that returns a  JSON object
  * @returns { JSON } { error, message}
 */

export type ErrorResponse = {
    error: string;
    message: string;
};

