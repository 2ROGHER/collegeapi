import { IAuth } from "../domain/interfaces/IAuth.interface";
import { IUser } from "../domain/interfaces/IUser.interface";
import { CreateUser, GetAllUsers } from "../domain/orm/User.orm";
import { IAuthController, IUserController } from "./interfaces";

export class UserController implements IUserController {
    async createUser(user: IUser): Promise<any> {
        try {
            return await CreateUser(user);
        } catch (error: any) {
            throw new Error("Erro creating user: " + error.message);
        }
    }

    getUserByName(name: string): Promise<any> {
        throw new Error("Method not implemented.");
    };
    
    udpateUserById(
        user: IUser,
        id: `${string}-${string}-${string}-${string}-${string}`
    ): Promise<any> {
        throw new Error("Method not implemented.");
    }

    deleteUserById(
        id: `${string}-${string}-${string}-${string}-${string}`
    ): Promise<any> {
        throw new Error("Method not implemented.");
    }

    public async getAllUsers(): Promise<any> {
        let response: any = await GetAllUsers();
        return response;
    }

    getUserById(
        id: `${string}-${string}-${string}-${string}-${string}`
    ): Promise<any> {
        throw new Error("Method not implemented.");
    }
}