import { Schema } from "mongoose";
import { IAuth } from "../domain/interfaces/IAuth.interface";
import { ICustomer } from "../domain/interfaces/ICustomer.interface";

import { IUser } from "../domain/interfaces/IUser.interface";
import { Login, Register } from "../domain/orm/Auth.orm";
import { LogError, LogSuccess, LogWarning } from "../utils/Logger";
import { IAuthController } from "./interfaces";
import { UUID } from "crypto";
import ClientEntity from "../domain/models/Customer.entity";

class AuthController implements IAuthController {
  public async login(auth: IAuth): Promise<any> {
    let response: any;

    // If "auth" exists, we need to execute login process.
    try {
      if (auth) {
        let data: any = await Login(auth);
        response = {
          token: data,
          message: `Welcome ${data}`,
        };
      } else {
        LogWarning(
          400,
          `Register needs Auth Entity`,
          "/api/v1/register"
        );
        response = {
          error: "Authentication Username & Password are needed",
          message: "Please, provide a username and password",
        };
      }   
       return response;

    } catch (error: any) {
      // TODO: Handle error properly.
      throw new Error(error.message);
    }

  }

  public async register(customer: ICustomer): Promise<any> {
    try {
      return await Register(customer);
    } catch (error: any) {
      //TODO: Handle error properly.
      throw new Error("Error registering customer: " + error.message);
    }
  }

  logout(): Promise<any> {
    throw new Error("Method not implemented.");
  }
}

export default AuthController;
