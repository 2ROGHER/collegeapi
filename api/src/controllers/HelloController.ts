import { IHelloController } from "./interfaces/index";

// Implements the IHelloController
export class HelloController implements IHelloController {
  // NO atributes
  public async getMessage(name?: string | undefined): Promise<any> {
    // A simple JSON response
    return {
      message: `Hello ${name}, Wellcome my API. Thanks for visit`,
    };
  };
};
