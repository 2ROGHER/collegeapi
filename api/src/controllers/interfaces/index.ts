import { IUser } from "../../domain/interfaces/IUser.interface";
import { ITask } from "../../domain/interfaces/ITask.interface";
import { IAuth } from "../../domain/interfaces/IAuth.interface";
import { ICustomer } from "../../domain/interfaces/ICustomer.interface";
import { UUID } from "crypto";

export interface IHelloController {
    getMessage(name?: string): Promise<any>;
    // TODO: new functionalities for the controller.
};

// Interface for UserController
export interface ICustomerController {
    //Define all controllere types for User
    getAllCustomers(page?: number, limit?: number): Promise<any>;

    // Get Client by ID
    getCustomerById(id: string): Promise<any>;

    // Delete Client by ID
    deleteCustomerById(id: string): Promise<any>;

    // Create Client
    createCustomer(customer: ICustomer): Promise<any>;

    //Update Client
    updateCustomer(customer: ICustomer, id: string): Promise<any>;

    // Get tasks by Client ID
    getTaskByCustomerId(id: string): Promise<any>;
};

// Interface to define to implements in TaskController.
export interface ITaskController {
    getAllTasks(id?: string): Promise<ITask>;

    getTaskByID(id: string): Promise<ITask>;

    deleteTaskByID(id: string): Promise<ITask>;

    createTask(task: ITask): Promise<any>;

    updateTask(task: ITask, id: string): Promise<any>;
};

// Inteface to implement the Auth Controller
export interface IAuthController {
    login(user: IUser): Promise<any>;

    register(customer: ICustomer): Promise<any>;

    logout(): Promise<any>;
};

// Interface to implement User Controller
export interface IUserController {
    getAllUsers(): Promise<any>;

    getUserById(id: UUID): Promise<any>;

    getUserByName(name: string): Promise<any>;

    createUser(user: Partial<IUser>): Promise<any>;

    udpateUserById(user: IUser, id: UUID): Promise<any>;

    deleteUserById(id: UUID): Promise<any>;
};
