export class TestController {
  public async response(message?: string): Promise<any> {
    return {
      message: "Testing a controller message: " + message,
    };
  }
}
