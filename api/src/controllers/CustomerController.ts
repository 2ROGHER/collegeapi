// Here we need to define the
import { IUser } from "../domain/interfaces/IUser.interface";
import { ICustomerController } from "./interfaces";

// We need to import the ORM to works with the data in db

import { ICustomer } from "../domain/interfaces/ICustomer.interface";

//import logger to show messages in console.
import { LogError, LogSuccess } from "../utils/Logger";
import {
    CreateCustomer,
    DeleteCustomerByID,
    GetAllCustomers,
    GetCustomerByID,
    GetTaskByCustomerID,
    UpdateCustomer,
} from "../domain/orm/Customer.orm";

export class CustomerController implements ICustomerController {
    public async getAllCustomers(page: number, limit: number): Promise<any> {
        let response: any = {};

        try {
            //Use ORM method to get all clients from database.
            return await GetAllCustomers(page, limit);
        } catch (error: any) {
            throw new Error(error.message);
        }
    }

    public async getCustomerById(id: string): Promise<any> {
        let response: any;
        console.log("id en controllers is" + id);
        try {
            if (!id) {
                LogError(
                    400,
                    "Get client by ID request failed id null or empty",
                    "/api/v1/clients/" + id
                );
                throw new Error("Id null or empty");
            } else {
                response = await GetCustomerByID(id);
                LogSuccess(
                    200,
                    "Get client by ID request succeeded",
                    "/api/v1/clients/" + id
                );
                return response;
            }
        } catch (error: any) {
            throw new Error(error.message);
        }
    }

    public async deleteCustomerById(id: string): Promise<any> {
        let response: any;
        try {
            // Delete the client by ID with ORM method in database.
            if (!id) {
                response = await DeleteCustomerByID(id);
                LogSuccess(
                    200,
                    "Client deleted successfully",
                    "/api/v1/clients/" + id
                );
                return response;
            } else {
                LogError(400, "ID not found" + id, "/api/v1/clients/" + id);
                throw new Error("Delete client by ID not found id: " + id);
            }
        } catch (error: any) {
            throw new Error(error.message);
        }
    }

    public async createCustomer(customer: ICustomer): Promise<any> {
        let response: any;

        try {
            if (!customer) {
                response = await CreateCustomer(customer);
                LogSuccess(
                    200,
                    "Customer created successfully",
                    "/api/v1/customers"
                );
                return response;
            } else {
                LogError(
                    400,
                    "Customer atributes are null or empty, can not create customer",
                    "/api/v1/customers"
                );
                // response = {
                //   message: "Can not create customer in database",
                //   code: 400,
                // }
                // throw new error.
                throw new Error(
                    "Customer atributes are null or empty, can not create customer"
                );
            }
        } catch (error: any) {
            throw new Error(error.message);
        }
    }

    public async updateCustomer(customer: ICustomer, id: string): Promise<any> {
        let response: any;
        try {
            if (!customer || !id) {
                response = await UpdateCustomer(customer, id);
                LogSuccess(
                    200,
                    "Customer updated successfully",
                    "/api/v1/customers"
                );
                return response;
            } else {
                // 1. throw new errro
                // 2. return response with o josn object
                LogError(
                    400,
                    "Customer or  id null or empty",
                    "api/v1/customers" + id
                );
            }
        } catch (error: any) {
            throw new Error(error.message);
        }
    }

    public async getTaskByCustomerId(id: string): Promise<any> {
        try {
            return await GetTaskByCustomerID(id);
        } catch (error: any) {
            LogError(500, error.message, "/api/Clients/tasks?id=" + id);
        }
    }
}
