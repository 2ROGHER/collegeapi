import { Json } from "sequelize/types/utils";
import ClientEntity from "../domain/models/Customer.entity";

export const pagination = async (page?: number, limit?: number) => {
  let ClientModel = ClientEntity();
  let response: any = {};

  let total = await ClientModel.countDocuments();
  response.totalPages = Math.ceil(total / (total === undefined ? 1 : total));
  response.currentPage = page;

  return response;
};
