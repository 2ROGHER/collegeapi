import axios from "axios";
import { LogError } from "../utils/Logger";

async function registerCustomerGitHub(accessToken: string): Promise<any> {
    
    try {
        let userResponse = await axios.post('https://api.github.com/user',{
            Headers: {
                Authorization: `token ${accessToken}`,
            }
        })
        console.log("user response: " + JSON.stringify(userResponse));

        return Promise.resolve(userResponse);
    } catch (error: any) {
        LogError(
            500,
            "Error registering customer from GitHub: " + error.message,
            "/api/v1/auth/github/"
        );
    }
}

export default registerCustomerGitHub;
