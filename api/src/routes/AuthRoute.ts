import { verifyToken } from "../middlewares/verifyToken.middleware";
import express, { Express, Request, Response } from "express";
import bcrypt from "bcrypt";
import axios from "axios";

import bodyParser from "body-parser";
import AuthController from "../controllers/AuthController";
import { IAuth } from "../domain/interfaces/IAuth.interface";
import { CustomerController } from "../controllers/CustomerController";
import { LogError, LogSuccess } from "../utils/Logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { ICustomer } from "../domain/interfaces/ICustomer.interface";
import ROLE from "../domain/types/Role.enum";
import dotenv from "dotenv";
import registerCustomerGitHub from "../services/register-user-github.service";

// import client_id from environment variagbles.
dotenv.config();
const CLIENT_ID = process.env.GITHUB_CLIENT_ID;
const CLIENT_SECRET = process.env.GITHUB_CLIENT_SECRET;

let jsonParser = bodyParser.json();

let authRouter = express.Router();

/**
 * When the customer that is not logged in yet. This dont be allowed to acces the api.
 * The customer needs to be logged in first to access the api.
 * However, the "register" action is allowed for every customer that needs to be logged in.
 */
authRouter
    .route("/login") // Note here we're going to change this for 'bodyParser'
    .post(jsonParser, async (req: Request, res: Response) => {
        let { username, password }: IAuth = req?.body;

        try {
            if (username && password) {
                let controller: AuthController = new AuthController();

                // Use IAuth methods
                let auth: IAuth = {
                    username: username,
                    password: password,
                };

                // Send to client with token JWT key authorie the user
                LogSuccess(
                    200,
                    "Authentication process completed successfully",
                    "/api/v1/auth/login"
                );
                return res.status(200).send(await controller.login(auth));
            } else {
                LogError(
                    req.statusCode as number,
                    "Authentication process failed",
                    req.url
                );
                res.status(400).send({
                    message:
                        "[ERROR USER DATA MISSING]: No user can be loged in",
                });
            }
        } catch (error: any) {
            return res.status(400).json({ error: error.message });
        }
    });

authRouter
    .route("/register")
    .post(jsonParser, async (req: Request, res: Response) => {
        let { name, lastName, email, phone, DOB, age, user }: ICustomer =
            req.body;

        // Cretea new customer object
        let newCustomer: any = {
            name,
            lastName,
            email,
            phone,
            DOB,
            age,
            user: {
                username: user.username,
                password: user.password,
                role: user.role || ROLE.CUSTOMER,
            },
        };
        let controller: AuthController = new AuthController();

        try {
            res.status(200).json(await controller.register(newCustomer));
            LogSuccess(200, "Customer registered successfully", req.url);
        } catch (error: any) {
            LogError(
                500,
                "Registration process failed: " + error.message,
                "/api/v1/auth/register"
            );
            res.status(500).json({ error: error.message });
        }
    });

authRouter
    .route("/me")
    .get(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;

        if (id) {
            let controller: CustomerController = new CustomerController();
            // Obtain repsonse from Controller.
            let response: any = await controller.getCustomerById(id);
            return res.status(200).json(response);
        } else {
            res.status(401).json({
                message: "You do not have permission to access this route.",
            });
        }
    });

// We need to get the call back request function from github server
authRouter.get("/github", async (req: Request, res: Response) => {
    res.redirect(
        `https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}`
    );
});

authRouter.get("/callback", async (req: Request, res: Response) => {
    const code = req.query.code as string;

    if (!code) {
        return res.status(400).send({ error: "No code provided" });
    }

    try {
        let token = await axios.post(
            'https://github.com/login/oauth/access_token',
            {
                client_id: CLIENT_ID,
                client_secret: CLIENT_SECRET,
                code: code,
            },
            {
                headers: {
                    Accept: "application/json;odata=",
                },
            }
        );
        let accessToken = token.data.access_token;

        LogSuccess(
            200,
            "Successfully authenticated into GitHub account",
            "/api/v1/auth/callback"
        );

        // send accest token to client to saveg

        
        res.status(200).json({ accessToken });
        
    } catch (error: any) {
        LogError(
            500,
            "Failed to authenticate with GitHub: " + error.message,
            "/api/v1/auth/callback"
        );
        res.status(500).json({ error: error.message });
    }
});

authRouter.get("/test/register", async (req: Request, res: Response) => {
    try {
        // Capturamos el token de autorización desde el header
        let accessToken = req.headers.authorization;

        // Verificamos si el token está presente
        if (!accessToken) {
            return res
                .status(400)
                .json({ error: "Authorization header is missing" });
        }

        // Removemos el prefijo "Bearer " del token
        if (accessToken.startsWith("Bearer ")) {
            accessToken = accessToken.slice(7, accessToken.length);
        }

        // Hacemos la solicitud a la API de GitHub con el token de acceso
        let userResponse = await axios.get("https://api.github.com/user", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                "X-GitHub-Api-Version": "2022-11-10", // Aseguramos que la versión de la API esté incluida
            },
        });

        // Respondemos con los datos del usuario
        res.json(userResponse.data);
    } catch (error: any) {
        console.error("Error fetching user data from GitHub:", error.message);
        res.status(500).json({ error: "Internal Server Error" });
    }

});
export default authRouter;
