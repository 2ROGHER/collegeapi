import express, { Response, Request } from "express";
// We need to import the Controller 
import { HelloController } from "../controllers/HelloController";
import { LogSuccess } from "../utils/Logger";

// Let's make the route to manage 
let helloRoute: any = express.Router();

helloRoute.route('/')
    .get(async (req: Request, res: Response) => {
        // 1. Get the query param that is passed throught URL
        let name: any = req?.query?.name;
        // 2. Lets create a Controller Instance
        let controller = new HelloController();
        // 3. Obtain the response
        const response = await controller.getMessage(name);
        // 4. LogSuccess 
        LogSuccess(res.statusCode, 'Request success in hello test route', req.url);
        // 4. Send the response to client
        return res.send(response);
    });

//export the route
export default helloRoute;
