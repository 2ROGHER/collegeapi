import express, {Express, Response, Request} from 'express';

// Let's make the viewRoute
let viewRouter: Express = express();

// We send the file in endpoint: 'http://localhost:8000/api/view'
viewRouter.get('/', (req: Request, res:Response, next) => {
    res.sendFile(__dirname.split("\\").splice(0,6).join("\\") + "\\public\\view.html");
});

// export route
export default viewRouter;