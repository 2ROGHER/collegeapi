import express, { Express, Request, Response } from "express";
import { TaskController } from "../controllers/TaskController";
import bodyParser from "body-parser";
import { ITask } from "../domain/interfaces/ITask.interface";
import { LogError, LogSuccess } from "../utils/Logger";
import mongoose from "mongoose";
import { verifyToken } from "../middlewares/verifyToken.middleware";

let taskRouter: Express | any = express.Router();
// Security Middlewares goes  here for this route
taskRouter.use(bodyParser.json());
taskRouter.use(bodyParser.urlencoded({ extended: true }));

/**
 * When the customer get all task from dasboard, we need that
 * customer has read-only permissions to access the data.
 * Wheather the customer becomes to user this can be create a task and
 * make any changes followed by that "admin role".
 */
taskRouter
    .route("/") // Get all task from database.
    .get(async (req: Request, res: Response) => {
        let controller: TaskController = new TaskController();
        let response = {};
        let task: ITask;

        try {
            response = await controller.getAllTasks();
            LogSuccess(
                200,
                "Get all task successfully completed.",
                "/api/tasks"
            );
            return res.status(200).json(response);
        } catch (error: any) {
            LogError(
                500,
                "GEt all task failed: " + error.message,
                "/api/tasks"
            );
            res.status(error.status).json({ error: error.message });
        }
    })
    .post(verifyToken, async (req: Request, res: Response) => {
        let controller = new TaskController();
        let response: ITask;
        let task: ITask = req.body;
        try {
            response = await controller.createTask(task);
            LogSuccess(200, "[Request] Create task successfully", "/api/tasks");
            // return res.status(200).json(response);
            return res.status(200).json(task);
        } catch (error: any) {
            LogError(
                500,
                "[Request] Get all tasks failed: " + error.message,
                "/api/tasks"
            );
            return res.status(401).json({ error: error.message });
        }
    })
    .put(verifyToken, async(req: Request, res: Response) => {
        // TODO: update implementation
    })
    .delete(verifyToken, async(req: Request, res: Response) => {
        // TODO: delete implementation
    });

export default taskRouter;
