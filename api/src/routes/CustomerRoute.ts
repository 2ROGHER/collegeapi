import express, { Express, Request, Response, Router } from "express";
// import body parser to read bodies from request
import bodyParser from "body-parser";
import { IUser } from "../domain/interfaces/IUser.interface";
import { CustomerController } from "../controllers/CustomerController";
import { LogError, LogInfo, LogSuccess } from "../utils/Logger";
import { ICustomer } from "../domain/interfaces/ICustomer.interface";

// This middleware allows you to restrict the url, until you register and get  a token properly. 🔐🔐
import { verifyToken } from "../middlewares/verifyToken.middleware";

let customerRoute: Router = express.Router();

// Use a denided jsonParser
let jsonParser = bodyParser.json();

// Use of middlewares
customerRoute.use(bodyParser.json());
customerRoute.use(bodyParser.urlencoded({ extended: true }));

// Let's create a router for it
customerRoute
  .route("/")
  .get(jsonParser, async (req: Request, res: Response) => {
    let id: any = req?.query?.id;
    let controller: CustomerController = new CustomerController();

    // Handle errors here.
    try {
      //pagination
      let page: any = req?.query?.page;
      let limit: any = req?.query?.limit;

      // Response
      return res.status(200).send(await controller.getAllCustomers(page, limit));
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  })
  .put(async (req: Request, res: Response) => {
    let id: string = req?.params?.id;
    let client: ICustomer = req?.body;

    let controller: CustomerController = new CustomerController();
    try {
      res.status(202).send(await controller.updateCustomer(client, id));
    } catch (error: any) {
      return res.status(500).json({ error: error.message });
    }
  })
  .post(async (req: Request, res: Response) => {
    let controller: CustomerController = new CustomerController();
    try {
      let client: ICustomer = req.body;
      await controller.createCustomer(client); //this method return a client object
      return res.status(201).json({ created: client });
    } catch (error: any) {
      return res.status(500).json({ error: error.message });
    }
  })
  .delete(async (req: Request, res: Response) => {
    let id: string = req?.params?.id;

    let controller: CustomerController = new CustomerController();

    try {
      return res.status(200).send(await controller.deleteCustomerById(id));
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  });

// Get client by ID route
customerRoute.route('/:id').get(async (req: Request, res: Response) => {
    let id: string = req?.params?.id;
    let controller: CustomerController = new CustomerController();
    try {
      return res.status(200).send(await controller.getCustomerById(id));
    } catch (error: any) {
      return res.status(500).json({ error: error.message });
    }
  });

customerRoute.route("/tasks").get(async (req: Request, res: Response) => {
  let id: string | any = req.query.id;
  let controller: CustomerController = new CustomerController();
  let response: any;
  try {
    response = await controller.getCustomerById(id);
    LogInfo(301, "[Request] ID is done " + id, "/api/tasks?id=");
    LogSuccess(
      200,
      "[Request] Get tasks by user ID  successfully",
      "/api/tasks?id=" + id
    );
    return res.status(200).json(response);
  } catch (error: any) {
    LogError(
      401,
      "[Request] Get tasks by client ID failed: " + error.message,
      "/api/users/tasks?id=" + id
    );
    res.status(401).json({ error: error.message });
  }
});

export default customerRoute;
