import express, { Request, Response} from 'express';
import { TestController } from '../controllers/TestController';

let testRoute = express.Router();

testRoute.get('/',async (req:Request, res: Response) => {
    let message: string = 'este es un mesanje';
    let controller =  new TestController();
    let response = await controller.response(message);
    return res.json(response);
});

export default testRoute;