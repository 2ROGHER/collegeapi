// Here goes all routes
import express, { Request, Response } from "express";
// Import all routes here
import helloRoute from "./HelloRoute";
import viewRouter from "./ViewRoute";
import userRouter from "./UserRoute";
import taskRouter from "./TaskRoute";
import authRouter from "./AuthRoute";
import customerRoute from "./CustomerRoute";
// LEt's make a express instance
let server = express();

// We need to craete the 'main' route to works
let rootRouter = express.Router();

// Activate for all request here the route in the link: 'http://localhost:8000/api'
rootRouter.get("/", async (req: Request, res: Response) => {
  res.send("Welcome to College API + Node.JS + Webpack + JS + TS + MongoDB");
});


// when the route is calling in '/' is redirect  to 'rootRouter'
server.use("/", rootRouter);

// ****** HERE ARE GOING ALL NO MAIN ROUTES ********

rootRouter.use("/hello", helloRoute);
rootRouter.use('/view', viewRouter); // returns the view HTML.
/**
  * Trust routes for the project.
 */
rootRouter.use('/users', userRouter);
rootRouter.use('/customers', customerRoute);
rootRouter.use('/tasks', taskRouter); // returns task opereations

// *** Authentication routes *** 
rootRouter.use("/auth", authRouter);

// Export the main route
export default rootRouter;
