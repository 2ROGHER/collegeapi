

import express, {Request, Response} from 'express';
import { UserController } from '../controllers/UserController';
import { verifyToken } from '../middlewares/verifyToken.middleware';
import { IUser } from '../domain/interfaces/IUser.interface';

let userRouter = express.Router();

userRouter.route('/')
    .get( async (req: Request, res: Response) => {
        let controller: UserController = new UserController();
        let response: any = await controller.getAllUsers();
        res.status(200).json(response);
    })
    .post(async (req: Request, res: Response) => {
        let controller: UserController = new UserController();

        let { _id, username, password, role }: IUser = req?.body;
        let newUser: any = {
            _id,
            username,
            password, 
            role,
        };

        try {
            res.status(200).send(await controller.createUser(newUser));    
        } catch (error: any) {
            res.status(400).json({ error: error.messsge });
        }
    });

export default userRouter;