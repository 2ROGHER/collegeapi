// clasess
function getTime(): number {
  // code here
  return 1;
}

const getLog = (
  name: string,
  lastname?: string,
  email: string = "roger mestanza 2"
): string => {
  // code here
  return "hola";
};

const car: { type: string; model: string; year: number } = {
  type: "ferrari",
  model: "avenger",
  year: 293,
};

const map: { [index: string]: number } = {};

(map.jack = 34), (map.fiorela = 234);

function divide({ divided, divisor }: { divided: number; divisor: number }) {
  return divided / divisor;
}

// types
type Negate = (value: number) => number;

class Car {
  name: string;
  lastname: string;

  constructor(name: string, lastname: string) {
    this.name = name;
    this.lastname = lastname;
  }
}

// protected: permite el acceso a los miembros de una clases desde la clase misma y objetos HEREDADOS DE ESTE

interface Point {
  x: number;
  y: number;
}

let partialPoint: Partial<Point> = {};

let requiredPoint: Required<Point> = {
  // laspropiedades del objeto son requeridos
  x: 10,
  y: 20,
};

const ubication: Record<string, number> = {
  bob: 234234.34,
  alice: 25235.23424,
};

let removedPoint: Omit<Point, "y"> = {
  x: 24253.23,
  // here the point 'y' was Omit in its definition
};

let pickedPoint: Pick<Point, "y"> = {
  y: 934232.342,
};

type PointGenerator = () => { x: number; y: number };
const point: ReturnType<PointGenerator> = {
  x: 10,
  y: 22,
};

type PointPrinter = (p: { x: number; y: number }) => void;
const pointer: Parameters<PointPrinter>[0] = {
  x: 10,
  y: 20,
};

// Generics
function createPair<S, T>(v1: S, v2: T): [S, T] {
  return [v1, v2];
}
console.log(createPair<string, number>("hola", 2)); // ['hola', 2]
