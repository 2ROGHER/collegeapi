// In this file we're going to create all logger messages to show  client
import chalk from 'chalk';

// LogSuccess message
export const LogSuccess = (
  code: number,
  message: string,
  url: string
): void => {
  console.log(
    `${chalk.green(`+[${code}]`)} | ${chalk.rgb(
      105,
      119,
      189
    )(`[message]: ${message}`)} | [url]: ${url}`
  );

};

// ErrorLog message
export const LogError = (code: number, message: string, url: string): void => {
console.log(
  `${chalk.red(`+[${code}]`)} | ${chalk.rgb(
    238,
    146,
    146
  )(`[message]: ${message}`)} | [url]: ${url}`
);};

// InfoLog messages
export const LogInfo = (code: number, message: string, url: string): void => {
  console.log(
  `${chalk.rgb(147, 175, 220)(`+[${code}]`)} | ${chalk.rgb(
    105,
    119,
    189
  )(`[message]: ${message}`)} | [url]: ${url}`);
};

// WarnLog message
export const LogWarning = (
  code: number,
  message: string,
  url: string
): void => {
 console.log(
   `${chalk.rgb(230, 190, 121)(`+[${code}]`)} | ${chalk.rgb(
     105,
     119,
     189
   )(`[message]: ${message}`)} | [url]: ${url}`
 );};
